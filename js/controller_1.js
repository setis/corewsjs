var controller = {
    debug: true,
    event: {
        lastTime: 0,
        timeout: 5000,
        event: null,
        on: function() {
            if (ws.api.readyState !== 1) {
                ws.connect();
            } else {
                controller.send({"cmd": "keepalive"});
            }
            console.info('status :' + ws.api.readyState);
        },
        handler: function(status) {
            if (status === true) {
                this.lastTime = new Date().getTime();
                clearTimeout(controller.event.event);
                controller.event.event = setTimeout(controller.event.handler, controller.event.timeout);
            } else {
                controller.event.on();

            }

        }
    },
    init: function() {
        ws.onopen = this.on.open;
        ws.onclose = this.on.close;
        ws.onmessage = this.on.handler;
        ws.onerror = this.on.error;
        ws.connect();
    },
    on: {
        open: function() {
            ws.connected = true;
            core.autch();
        },
        handler: function(d) {
            if (d.data === null) {
                return;
            }
            controller.event.handler(true);
            controller.cmd(d.data);
        },
        close: function() {
            ws.connected = false;
//            console.info(ws.api.readyState);
        },
        error: function(e) {
            ws.connected = false;
            console.error(e);
            controller.event.handler(false);
        }
    },
    cmd: function(d) {
        var result = true;
        try
        {
            var data = JSON.parse(d);
        }
        catch (e)
        {
            result = false;
        }
        //console.info(result);
        if (result) {
            if (this.debug) {
                console.info(d);
            }
            if (this.packets[data.type] === undefined) {
                if (this.debug) {
                    console.error(data.type);
                }
            } else {
                controller.packets[data.type](data.data, data.time);
            }
        }
    },
    packets: {
        UserList: function(data) {
            var i = 0, app = "";
            for (var sid in data) {
                var s = data[sid];
                app += templates.userlist(s);
                cache.userlist_add(s);
            }
            controller.userlangth();
            $("#userlist").html(app);
        },
        UserList_add: function(data) {
            if (!cache.userlist_is(data.sid)) {
                $("#userlist").append(templates.userlist(data));
                cache.userlist_add(data);
            } else {
                $("#userlist-" + data.sid).remove();
                $("#userlist").append(templates.userlist(data));
            }
            controller.userlangth();
            if (typeof useradmin === "function")
                useradmin();
        },
        UserList_del: function(sid) {
            $("#userlist-" + sid).remove();
            cache.userlist_del(sid);
            controller.userlangth();
        },
        getHistory: function(data, time) {
            var appm = "";
            for (var msg_id in data) {
                var l = data[msg_id];
                appm += templates.messages(l, controller.time(time));
            }
            $("#messages").append(appm);
        },
        msg_add: function(data, time) {
            $("#messages").append(templates.messages(data, controller.time(time)));
            $('#messages').animate({scrollTop: $('#messages')[0].scrollHeight});
            if (typeof useradmin === "function")
                useradmin();
        },
        msg_del: function(data) {
            for (var sid in data) {
                $("#messages-" + data[sid]).remove();
            }
        },
        channel_connect: function(data) {

        },
        evel: function(data) {
            eval(data);
        },
        role: function(data) {
            $('#userlist-' + data.sid + " > div:first-child").attr("class", "sus" + data.role);
        },
        keepalive: function(data) {

        },
        info: function(data) {
            $('#infouser').html(data.info);
        }
    },
    send: function(d) {
        if (d)
            ws.send(JSON.stringify(d));
    },
    time: function(time) {
        var date = new Date(time * 1000);
        return date.getHours() + ':' + date.getMinutes();
    },
    userlangth: function() {
        document.getElementById("susrs").innerHTML = Object.keys(cache.ram.userlist).length;
    }
};