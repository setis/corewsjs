var controller = {
    sentBytes: 0,
    recvBytes: 0,
    statrt:null,
    send: function(packet) {
        var s = $.toJSON(packet);
        this.sentBytes += s.length;
        try {
            ws.send(s);
        }
        catch (e) {
            console.error(e);
        }
    },
    recv: function(e) {
        if (e.data === null) {
            return;
        }
        this.recvBytes += e.data.length;
        return $.parseJSON(this.urldecode(e.data));
    },
    urlencode: function(s) {
        if (typeof encodeURIComponent !== 'undefined') {
            return encodeURIComponent(s).replace(new RegExp('\\+', 'g'), '%20');
        }
        return escape(s).replace(new RegExp('\\+', 'g'), '%20');
    },
    urldecode: function(s) {
        return unescape(s).replace(new RegExp('\\+', 'g'), ' ');
    },
    size: function(x) {
        if (x >= 1073741824) {
            return (Math.floor(x / 1073741824 * 100) / 100) + ' Gb';
        }
        if (x >= 1048576) {
            return (Math.floor(x / 1048576 * 100) / 100) + ' Mb';
        }
        if (x >= 1024) {
            return (Math.floor(x / 1024 * 100) / 100) + ' Kb';
        }
        return (x) + ' B';
    },
    start:function(){
        
    }
};