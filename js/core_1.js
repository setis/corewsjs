"use strict";
var useradmin;
var core = {
    url: null,
    name: null,
    family: null,
    gorod: null,
    nick: null,
    chcontrol: null,
    tema: "Тема вебинара",
    vedyhii: "Ведущий",
    avtorz: null,
    strip_tag: false,
    channel: function(idurl, callback) {
        core.loading("/json/channel/channel" + idurl + ".json?" + Math.random(), 1, 0, null, callback);
    },
    channel_ws: function(id, callback) {
        ws.send();
    },
    event: function() {
        core.url = document.location.href.split('/');
        switch (core.url[3]) {
            case "administrator":
                alert("Шо, правда админ?");
                return false;
                break
            case "demo":
                core.loadjscssfile("/css/management.css", "css");
                core.loadjscssfile("/js/management.js", "js");
                break
            default:
                break
        }
        var callback = function(object) {
            var drds = JSON.parse(object);
            if (drds.check === "false") {
                /* вебинара не существует */
                document.getElementById("webinar").style.display = "none";
                document.getElementById("regster").style.display = "none";
                document.getElementById("nonewebinar").style.display = "block";
                return false;
            } else {
                /* вебинар существует */
                if (core.get_cookie('name') === null) {
                    /* пользователь не авторизован */
                    document.getElementById("webinar").style.display = "none";
                    document.getElementById("nonewebinar").style.display = "none";
                    document.getElementById("regster").style.display = "block";
                } else {
                    /* пользователь авторизован */
                    document.getElementById("regster").style.display = "none";
                    document.getElementById("nonewebinar").style.display = "none";
                    document.getElementById("webinar").style.display = "block";
                    core.webinar.event();
                }
            }
        };
        core.channel(core.comnata(), callback);
    },
    get_cookie: function(cookie_name) {
        var results = document.cookie.match('(^|;) ?' + cookie_name + '=([^;]*)(;|$)');
        if (results)
            return (unescape(results[2]));
        else
            return null;
    },
    add_cookie: function() {
        core.name = core.get_cookie('name');
        core.family = core.get_cookie('family');
        core.gorod = core.get_cookie('gorod');
        core.nick = core.name + " " + core.family + ' (' + core.gorod + ')';
    },
    register: function(sprv) {
        var reslt = true;
        var fig = "";
        var empty = [];
        if (sprv === "all") {
            empty = ["Имя", "Город"];
        } else if (sprv === "imya") {
            empty[0] = "Имя";
        } else if (sprv === "grd") {
            empty[1] = "Город";
        }
        var smvl = 3;
        var id = 0, lenghta = empty.length;
        for (; id < lenghta; id++) {
            var pds = document.getElementById("pds" + id);
            if (empty[id] !== undefined) {
                if (document.getElementById("in" + id).value.length < smvl) {
                    pds.style.display = "inline-block";
                    pds.style.color = "#cc0000";
                    pds.innerHTML = empty[id] + " не менее " + smvl + " символов";
                    reslt = false;
                } else {
                    pds.style.color = "green";
                    pds.innerHTML = "Отлично";
                }
            }
        }
        var namfamil = document.getElementById("in0").value;
        var nf = namfamil.split(" ");
        var pds = document.getElementById("pds0");
        if (nf[0].length > 2) {
            fig += "imya=" + nf[0] + "&";
            pds.style.color = "green";
            pds.innerHTML = "Отлично";
        } else {
            pds.parentNode.style.display = "inline-block";
            pds.style.color = "#cc0000";
            pds.innerHTML = "Имя не менее 3 символов";
            reslt = false;
        }

        var pds = document.getElementById("pdsf");
        if (!nf[1]) {
            pds.parentNode.style.display = "inline-block";
            pds.style.color = "#cc0000";
            pds.innerHTML = "Вы не указали Фамилию";
            //reslt = false;
            //return false;
        }
        if (nf[1]) {
            if (nf[1].length > 2) {
                fig += "family=" + nf[1] + "&";
                pds.style.color = "green";
                pds.innerHTML = "Отлично";
            } else {
                pds.parentNode.style.display = "inline-block";
                pds.style.color = "#cc0000";
                pds.innerHTML = "Фамилия не менее 3 символов";
                //reslt = false;
                //return false;
            }
        }
        if (sprv === "all" && reslt === true) {
            // Имя и Фамилия
            // gorod
            var grod = document.getElementById("in1").value;
            fig += "gorod=" + grod;
            console.info("[Авторизация]: " + nf[0] + " " + nf[1] + " " + grod);
            if (nf[1] === undefined || nf[1] === "" || nf[1] === null || nf[1] === " ")
                nf[1] = "_";
            if (grod === undefined || grod === "" || grod === null || grod === " ")
                grod = "_";
            document.cookie = "name=" + nf[0];
            document.cookie = "family=" + nf[1];
            document.cookie = "gorod=" + grod;
            core.event();
            core.autch();
        }
    },
    exit: function() {
        document.cookie = "name" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie = "family" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        document.cookie = "gorod" + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        controller.send({"cmd": "logout"});
        controller.send({"cmd": "channel_disconnect", data: core.comnata("1")});
        document.getElementById("userlist").innerHTML = "";
        document.getElementById("messages").innerHTML = "";
        core.event();
    },
    autch: function() {
        if (core.get_cookie('name') !== null) {
            controller.send({"cmd": "channel_connect", data: core.comnata("1")});
            //controller.send({"cmd": "channel_connect", data: "0"});
            console.info('controller.send({"cmd": "channel_connect", data: '+core.comnata("1").toString()+'})');
            controller.send({"cmd": "login", "data": {"name": core.get_cookie('name'), "family": core.get_cookie('family'), "city": core.get_cookie('gorod')}});
            controller.send({"cmd": "getUserList"});
            controller.send({"cmd": "getHistory"});
        }
        //  controller.send({"cmd": "login", "data": {"name": "Иван", "family": "Петрович", "city": "Уфа"}});
        //  controller.send({"cmd":"channel_connect","data":"1"});
    },
    comnata: function(id) {
        if (id === undefined)
            var idurl = "";
        else
            var idurl = "0";
        switch (core.url[3]) {
            case undefined:
                return idurl;
                break
            case " ":
                return idurl;
                break
            case "":
                return idurl;
                break
            default:
                return idurl = core.url[3];
                break
        }
    },
    heightblok: function() {
        var hsblk = document.getElementsByClassName("widblok")[0].clientHeight;
        var chats = hsblk - 100;
        if (chats > 398) {
            document.getElementById("messages").style.height = chats + "px";
        } else {
            document.getElementById("messages").style.height = "398px";
        }
        var userhg = hsblk - 210 - 80;
        if (userhg > 211) {
            document.getElementById("userlist").style.height = userhg + "px";
        } else {
            document.getElementById("userlist").style.height = "211px";
        }
    },
    loading: function(id, cache, type, data, callback) {
        var xmlhttp = null;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState === 4) {
                if (xmlhttp.status === 200) {
                    var result = xmlhttp.responseText;
                    if (callback) {
                        callback(result, true, cache);
                    }
                } else {
                    callback(xmlhttp.responseText, false);
                }
            }
        };
        switch (type) {
            case 0:
                xmlhttp.open("GET", id, true);
                xmlhttp.send();
                break
            case 1:
                xmlhttp.open("POST", id, true);
                xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                xmlhttp.send(data);
                break
        }
    },
    webinar: {
        event: function() {
            controller.init();
            core.heightblok();
            core.add_cookie();
            core.informer();
            document.getElementById("usname").innerHTML = core.name + " " + core.family;
            core.loading("/json/channel/channel" + core.comnata() + ".json?" + Math.random(), 1, 0, null, function(object) {
                var i = 0, drs = JSON.parse(object);
                document.getElementById("webtema").innerHTML = drs.name;
                document.getElementById("leading").innerHTML = drs.leading;
                document.getElementById("wbavaus").setAttribute("src", "/img/avatara/" + drs.avatara + ".png");
                for (i in drs.channel) {
                    document.getElementById("tbbr").innerHTML += "<div class=\"typebtrd\" data-type='" + drs.channel[i] + "' onclick='core.webinar.typebitr(this);'>" + i + "p</div>";
                }
                jwplayer("myElement").setup({
                    file: "rtmp://144.76.138.117:1935/live/test" + core.comnata(),
                    height: 360,
                    width: 640,
                    autoStart: 'true',
                    primary: 'flash'
                });
                var t;
                var timer = 5000;
                jwplayer().onIdle(function() {
                    t = setTimeout("jwplayer('myElement').play();", timer);
                    console.info("[jwPlayer]: onIdle play");
                });
                // jwplayer('myElement').onPlay(function() {alert('it has started')});
                /*
                jwplayer().onPause(function() {
                    t = setTimeout("jwplayer('myElement').play();", timer);
                    console.info("[jwPlayer]: onPause play");
                });
                */
                console.info("[Запуск трансляции]: " + "rtmp://144.76.138.117:1935/live/test" + core.comnata());
            });
        },
        left: {
            leftblok1: function(id) {
                document.getElementById("blm1").style.display = "block";
                document.getElementById("blm2").style.display = "none";
                id.className = "ychastak";
                id.parentNode.nextSibling.children[0].className = "ychastdf";
            },
            leftblok2: function(id) {
                return false;
                document.getElementById("blm1").style.display = "none";
                document.getElementById("blm2").style.display = "block";
                id.className = "ychastak";
                id.parentNode.previousSibling.children[0].className = "ychastdf";
            },
            editnone: function(id) {
                if (id.className === "headrbluedtop") {
                    id.className = "headrblued";
                    id.nextSibling.style.display = "none";
                    return false;
                }
                var allbl = id.parentNode.getElementsByClassName("edblk");
                for (var i in allbl) {
                    if (allbl[i].outerHTML !== undefined)
                        allbl[i].style.display = "none";
                }
                var btmel = id.parentNode.getElementsByClassName("headrbluedtop");
                for (var l in btmel) {
                    btmel[l].className = "headrblued";
                }
                id.nextSibling.style.display = "block";
                id.className = "headrbluedtop";
            }
        },
        centr: {
            video: function(id) {
                id.className = "gstrv";
                id.nextSibling.className = "gstrvdf";
            },
            doska: function(id) {
                return false;
                id.className = "gstrv";
                id.previousSibling.className = "gstrvdf";
            }
        },
        typebitr: function(id) {
            var btr = Number(id.innerHTML.replace(/\D+/g, ""));
            jwplayer().load([{file: id.getAttribute("data-type")}]);
            //jwplayer().load([{file: "http://content.bitsontherun.com/videos/3XnJSIm4-640.mp4"}]);
            //jwplayer().load([{file: "rtmp://144.76.138.117:1935/live/test", image: "/img/video/none.jpg"}]);
            var i = 0, btms = id.parentNode.getElementsByClassName("typebtra");
            var btmsn = btms.length;
            for (; i < btmsn; i++) {
                btms[i].className = "typebtrd";
            }
            id.className = "typebtra";
            console.info("[Качество трансляции]: " + btr);
        }
    },
    classobj: {
        hasClass: function(ele, cls) {
            return ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
        },
        addClass: function(ele, cls) {
            if (!this.hasClass(ele, cls))
                ele.className += " " + cls;
        },
        removeClass: function(ele, cls) {
            if (this.hasClass(ele, cls)) {
                var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
                ele.className = ele.className.replace(reg, ' ');
            }
        }
    },
    chat: {
        href: {
            click: function(id) {
                var inh = id.nextSibling;
                if (inh.style.display === "block")
                    inh.style.display = "none";
                else
                    inh.style.display = "block";
            },
            entr: function(id) {
                document.getElementById('inputMessage').value += id.value;
                id.value = "";
                id.parentNode.style.display = "none";
            }
        },
        keypentr: function(id) {
            var text = id.value;
            if (text === "" || text === " " || text === undefined)
                return false;
            //controller.send({"cmd": "msg", "data": {"msg": id.value, "from": {"sid": "123", "name": core.nick, "public": true}, "to": {"sid": "456", "name": ""}}});
            controller.send({"cmd": "msg", "data": {"msg": text, "from": {"sid": "123", "name": core.nick, "public": true}}});
            id.value = null;
        },
        submit: function() {
            var textchat = document.getElementById("inputMessage");
            var text = textchat.value;
            if (text === "" || text === " " || text === undefined)
                return false;
            //controller.send({"cmd": "msg", "data": {"msg": textchat.value, "from": {"sid": "123", "name": core.nick, "public": true}, "to": {"sid": "456", "name": ""}}});
            controller.send({"cmd": "msg", "data": {"msg": text, "from": {"sid": "123", "name": core.nick, "public": true}}});
            textchat.value = "";
            textchat.focus();
        },
        smiles: function(id) {
            var textchat = document.getElementById('inputMessage');
            textchat.value += 'smile' + id;
            textchat.focus();
            textchat.setSelectionRange(textchat.value.length, textchat.value.length);
        }
    },
    infwbr: {
        start: function(id) {
            document.getElementById("infwbr").style.display = "block";
            document.getElementById("inhtml").innerHTML = id;
        },
        exit: function() {
            document.getElementById("infwbr").style.display = "none";
            document.getElementById("inhtml").innerHTML = "";
        }
    },
    messagus: {
        start: function(id) {
            document.getElementById("messagblk").style.display = "block";
            document.getElementById("msginhtml").innerHTML = id;
        },
        exit: function() {
            document.getElementById("messagblk").style.display = "none";
            document.getElementById("msginhtml").innerHTML = "";
        }
    },
    keycheck: function(e) {
        var charCode = (navigator.appName === "Netscape") ? e.witch : e.keyCode;
        if (charCode === 116) {
            controller.send({"cmd": "logout"});
            controller.send({"cmd": "channel_disconnect", data: core.comnata("1")});
            e.keyCode = 0;
        }
        return false;
    },
    pass: {
        start: function() {
            var tmpl = "введите логин: <div class='wb2m'><input id='vslog' type='text'></div> введите пароль: <div class='wb2m'><input type='text' id='vspas' onkeydown='if (event.keyCode === 13){ core.pass.submit(); return false;}'></div><div class='wb2m'><input onclick='core.pass.submit();' type='submit'></div>";
            core.messagus.start(tmpl);
        },
        submit: function() {
            var lg = document.getElementById("vslog").value;
            var pg = document.getElementById("vspas").value;
            controller.send({"cmd": "sudo", "data": {"login": lg, "password": pg}});
            core.messagus.exit();
        }
    },
    loadjscssfile: function(filename, filetype, cache) {
        if (cache === undefined)
            cache = "";
        else
            cache = "?" + Math.random();
        switch (filetype) {
            case "js":
                var fileref = document.createElement('script');
                fileref.setAttribute("type", "text/javascript");
                fileref.setAttribute("src", filename + cache);
                break
            case "css":
                var fileref = document.createElement("link");
                fileref.setAttribute("rel", "stylesheet");
                fileref.setAttribute("type", "text/css");
                fileref.setAttribute("href", filename + cache);
                break
        }
        if (typeof fileref !== "undefined")
            document.getElementsByTagName("head")[0].appendChild(fileref);
    },
    informer: function() {
        var idurl = "";
        if (core.url[3] !== undefined)
            idurl = core.url[3];
        else
            idurl = "";
        core.loading("/json/event/ev" + idurl + ".json?" + new Date().getTime(), 1, 0, null, function(object) {
            if (object !== "") {
                var dats = JSON.parse(object);
                if (dats.name === "") {
                    document.getElementById("webtema").innerHTML = core.tema;
                } else
                    document.getElementById("webtema").innerHTML = dats.name;
                if (dats.leading === "") {
                    document.getElementById("leading").innerHTML = core.vedyhii;
                } else
                    document.getElementById("leading").innerHTML = dats.leading;
                document.getElementById("wbavaus").setAttribute("src", "/img/avatara/" + dats.avatara + ".png");
                document.getElementById("infoblok").innerHTML = dats.infoblok;
                console.info("[Информер]: ");
                document.getElementById("redinfo").innerHTML = "";
                if (dats.reload !== undefined && dats.reload !== "") {
                    window.location = "http://" + dats.reload;
                }
                if (dats.infolitebox !== undefined && dats.infolitebox !== "") {
                    core.infwbr.start(dats.infolitebox);
                } else {
                    core.infwbr.exit();
                }
                if(dats.chat === ""){
                    core.chcontrol = null;
                } else {
                    core.chcontrol = "1";
                    document.getElementById("messages").innerHTML = "";
                }
                
                //core.heightblok();
            }
            setTimeout(core.informer, 5000);
        });
    }
};
if (document.addEventListener) {
    document.addEventListener("DOMContentLoaded", core.event, false);
} else if (document.attachEvent) {
    window.attachEvent('onload', core.event);
}
document.onkeydown = function() {
    core.keycheck(event);
};