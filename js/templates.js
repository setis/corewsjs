var templates = {
    userlist: function(s) {
        return '<div id="userlist-' + s.sid + '"><div class="sus' + s.role + '"></div><a href="#" onclick="return false;">' + s.name + ' ' + s.family + ' ' + s.city + ' ' + '</a><div class="musrls"></div></div>';
    },
    messages: function(s, time) {
        if (s.to === undefined) {
            s.to = {
                sid: "",
                name: ''
            };
        }
        var meggag = maxlength(s.msg);
        meggag = htmlspecialchars(meggag);
        var key = ['smile0', 'smile1', 'smile2', 'smile3', 'smile4'];
        for (var i in key) {
            meggag = meggag.replace(new RegExp(key[i], 'g'), "<img src='/img/smile/" + i + ".gif'>");
        }
        if (core.chcontrol === null)
            return '<div id=' + s.msg_id + '><div>' + time + ' </div><div><a href="#" onclick="return false">' + s.from.name + '</a></div><div> ' + s.to.name + ' ' + meggag + '</div><div class="ustadmp"></div></div>';
        else
            return "";
    }
};

function htmlspecialchars(s) {
    if (s === null) {
        return 'null';
    }
    if(core.strip_tag === false){
        return s;
    }
        var preg = /((http:\/\/.+)[\s]?)/gi;
        if (s.match(preg)) {
            if (false) {
                var urls = s.match(preg);
                var shtml = "<a target='_blank' href='" + urls + "'>" + urls + "</a>";
                s = s.replace(s.match(preg), shtml);
            } else {
                var urls = s.match(preg);
                var shtml = " <span>ссылка</span> ";
                s = s.replace(s.match(preg), shtml);
            }
        }
    var r = s;
    r = r.replace(new RegExp('&', 'g'), '&amp;');
    r = r.replace(new RegExp('"', 'g'), '&quot;');
    r = r.replace(new RegExp('\'', 'g'), '&#039;');
    r = r.replace(new RegExp('<', 'g'), '&lt;');
    r = r.replace(new RegExp('>', 'g'), '&gt;');
    return r;
}

function maxlength(t) {
    if (t.length > 50) {
        return t = t.slice(0, 50) + '…';
    } else {
        return t;
    }
}