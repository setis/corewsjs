var ws = {
    url: {
//        ws: 'ws://webinars.ruelsoft.info:8047/Core',
        ws: 'ws://studio-devel.com:8047/Core',
        //ws: 'ws://' + document.domain + '/Core',
        comet: 'http://webinars.ruelsoft.info/WebSocketOverCOMET/?_route=Core',
        //comet: 'http://studio-devel.com/WebSocketOverCOMET/?_route=Core',
        //comet: 'http://' + document.domain + '/WebSocketOverCOMET/?_route=Core',
        polling: 'http://webinars.ruelsoft.info/WebSocketOverCOMET/?_route=Core'
        //polling: 'http://studio-devel.com/WebSocketOverCOMET/?_route=Core'
                //polling: 'http://' + document.domain + '/WebSocketOverCOMET/?_route=Core'
    },
    connected: function() {
        if (this.api.readyState === 1) {
            return true;
        }
        return false;
    },
    connect: function() {
        if (this.api.readyState === 1)
        {
            if (this.api.readyState !== WebSocket.CLOSED) {
                return;
            }
            else
            {
                console.info("* Trying to reconnect...");
            }
        } else {
            this.api.init();
            console.info("* Connecting...");
        }
    },
    onopen: function() {
        console.info('connect');
    },
    onmessage: function(d) {
        if (d.data === null) {
            return;
        }
        console.info(d.data);
    },
    onclose: function() {
        console.info('close');
    },
    onerror: function(e) {
        console.error(e);
    },
    send: function(data) {
        return this.api.send(data);
    },
    api: {
        /**
         * Статус WebSocket соединения
         */
        include: {
            flash: '/js/flash.swf',
            fabridge: '/js/fabridge.js',
            swfobject: '/js/swfobject.js',
            wsFlash: '/js/websocket_flash.js',
            comet: '/js/websocket_comet.js',
            polling: '/js/websocket_polling.js'
        },
        readyState: 0,
        bufferedAmount: 0,
        readyCallbacks: [],
        interval: [],
        /**
         * WebSocket соединение
         */
        resource: null,
        close: function() {
            if (this.resource) {
                this.resource.close();
            }
        },
        ready: function(c)
        {
            if (this.readyState === 1) {
                c();
            }
            else
            {
                this.readyCallbacks.push(c);
            }
        },
        /**
         * Отправка данных на сервер
         */
        send: function(data) {
            if (this.resource) {
                if (ws.api.readyState === 1) {
                    this.resource.send(data);
                } else {
                    this.onerror('нет соединение');
                }

            }
        },
        // соединение с сервером установлено
        onopen: function() {
            ws.api.readyState = 1;
            if (ws.onopen) {
                ws.onopen();
            }
            var c;
            while (c = ws.api.readyCallbacks.pop()) {
                c();
            }
        },
        // получен новый пакет данных
        onmessage: function(e) {
            if (ws.onmessage) {
                ws.onmessage(e);
            }
        },
        // соединение с сервером закрыто
        onclose: function()
        {
            ws.api.readyState = 3;
            if (ws.onclose) {
                ws.onclose();
            }
        },
        onerror: function(e) {
            if (ws.onerror) {
                ws.onerror(e);
            }
        },
        /**
         * Загрузка файла
         */
        loadFile: function(file) {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = file;
            var c = document.head || document.body;
            c.appendChild(script);
        },
        /**
         * Загрузка драйвера
         */
        loadEmulator: function(type, onLoaded) {

            if (type === 'flash') {   // загружаем флешовый эмулятор

                WebSocket__swfLocation = this.include.flash;  // путь к swf файлу

                if (!("FABridge" in window))
                    this.loadFile(this.include.fabridge);   // если не загружен FABridge, загружаем

                if (!("swfobject" in window))
                    this.loadFile(this.include.swfobject); // если не загружен swfobject, загружаем

                this.interval[0] = setInterval(function() {
                    var self = ws.api;
                    if (("FABridge" in window) && ("swfobject" in window)) {
                        clearInterval(self.interval[0]);
                        self.loadFile(self.include.wsFlash);
                    }
                }, 10);

            } else if (type === 'comet') {    // загружаем comet эмулятор

                this.loadFile(this.include.comet);

            } else if (type === 'polling') {   // загружаем long-poling эмулятор

                this.loadFile(this.include.polling);

            } else {
                console.error('Error loadEmulator');
                return;
            }

            this.interval[1] = setInterval(function() {
                if ("WebSocket" in window) {
                    clearInterval(ws.api.interval[1]);
                    onLoaded();
                }
            }, 5);
        },
        /**
         * Проверка на доступность драйверов
         */
        loadDriver: function(driver, onError) {

            if (!driver)
                onError();
            var loaded = false;
            switch (driver) {

                case 'ws'     :
                    loaded = this.WSDriver();
                    break;

                case 'comet'  :
                    loaded = this.CometDriver();
                    break;

                case 'polling' :
                    loaded = this.PollingDriver();
                    break;
            }

            if (!loaded) {
                onError();
            }
        },
        /**
         * Проверка на доступность нативного WebSocket или Flash плеера
         */
        WSDriver: function() {

            if ("WebSocket" in window) {
                this.socket(ws.url.ws);
                return true;
            }

            var x, flashinstalled = 0;
            var MSDetect = false;

            if (navigator.plugins && navigator.plugins.length) {
                x = navigator.plugins["Shockwave Flash"];
                if (x) {
                    flashinstalled = 2;
                } else {
                    flashinstalled = 1;
                }

                if (navigator.plugins["Shockwave Flash 9.0"] || navigator.plugins["Shockwave Flash 10.0"] || navigator.plugins["Shockwave Flash 10.1"]) {
                    flashinstalled = 2;
                }

            } else if (navigator.mimeTypes && navigator.mimeTypes.length) {

                x = navigator.mimeTypes['application/x-shockwave-flash'];
                if (x && x.enabledPlugin)
                    flashinstalled = 2;
                else
                    flashinstalled = 1;
            } else {
                MSDetect = true;
            }

            console.log(navigator.plugins);
            console.log(navigator.mimeTypes);

            if (flashinstalled !== 2 && MSDetect === true) {
                var iframe = document.createElement('iframe');
                with (iframe.style) {
                    left = top = "-100px";
                    height = width = "1px";
                    position = 'absolute';
                    display = 'none';
                }
                document.body.appendChild(iframe);
                var win;
                if (typeof (iframe.contentWindow) !== 'undefined') {
                    win = iframe.contentWindow.window;
                } else if (iframe.window) {
                    win = iframe.window;
                }

                if (win) {
                    win.flashinstalled = 0;
                    win.document.writeln("<html><body>");
                    win.document.writeln('<script LANGUAGE="VBScript">');
                    win.document.writeln('on error resume next');
                    win.document.writeln('For i = 1 to 11');
                    win.document.writeln('If Not(IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash." & i))) Then');
                    win.document.writeln('Else');
                    win.document.writeln('flashinstalled2 = 2');
                    win.document.writeln('End If');
                    win.document.writeln('Next');
                    win.document.writeln('If flashinstalled2 = 0 Then');
                    win.document.writeln('flashinstalled2 = 1');
                    win.document.writeln('End If');
                    win.document.writeln('</script>');
                    win.document.writeln('<script type="text/javascript">');
                    win.document.writeln('flashinstalled = flashinstalled2');
                    win.document.writeln('</script>');
                    win.document.writeln("</body></html>");
                    flashinstalled = win.flashinstalled;

                }
            }

            if (flashinstalled === 2) {
                this.loadEmulator('flash', function() {
                    ws.api.socket(ws.api.url.ws);
                });
                return true;
            }
            return false;
        },
        /**
         * Проверка на доступность драйвера comet
         */
        CometDriver: function() {

            if ("WebSocket" in window && ("WebSocketServicePrivider" in window) && WebSocketServicePrivider === 'comet') {
                ws.api.socket(ws.api.url.comet);
            } else {
                this.loadEmulator('comet', function() {
                    ws.api.socket(ws.api.url.comet);
                });
            }

            return true;
        },
        /**
         * Проверка на доступность драйвера long-polling
         */
        PollingDriver: function() {

            if ("WebSocket" in window && ("WebSocketServicePrivider" in window) && WebSocketServicePrivider === 'polling') {
                ws.api.socket(ws.api.url.polling);
            } else {
                this.loadEmulator('polling', function() {
                    ws.api.socket(ws.api.url.polling);
                });
            }
            return true;
        },
        /**
         * Иннициализируем вебсокет
         */
        socket: function(url) {
            this.resource = new WebSocket(url);
            this.resource.onopen = this.onopen;
            this.resource.onmessage = this.onmessage;
            this.resource.onclose = this.onclose;
            this.resource.onerror = this.onerror;
        },
        init: function() {
            var priority = [];
            var i = 0;
            for (var type in  ws.url) {
                priority[i] = type;
                i++;
            }
            ;
            var self = ws.api;
            if (typeof (priority[0]) !== undefined) {
                //console.info('load->', priority[0]);
                this.loadDriver(priority[0], function() {
                    console.error('not loaded:'.priority[0]);
                    if (typeof (priority[1]) !== undefined) {

                        self.loadDriver(priority[1], function() {
                            console.error('not loaded:'.priority[1]);
                            if (typeof (priority[2]) !== undefined) {

                                self.loadDriver(priority[2], function() {
                                    console.error('not loaded:'.priority[2]);
                                });
                            }
                        });
                    }
                });
            }
        }
    }

};